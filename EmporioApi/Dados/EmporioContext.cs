﻿using Microsoft.EntityFrameworkCore;
using EmporioApi.Models;


namespace EmporioApi.Dados
{
            public class EmporioContext : DbContext
        {
            public EmporioContext (DbContextOptions<EmporioContext> options) : base(options)
            {
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<ItemEstoque>()
                    .HasKey(ac => new { ac.Bebida, ac.Estoque });
            }

        public EmporioContext(DbSet<Bebida> bebidas, DbSet<Estoque> estoques, DbSet<Formula> formulas)
        {
            Bebidas = bebidas;
            Estoques = estoques;
            Formulas = formulas;
        }

        public DbSet<Bebida> Bebidas { get; set; }
            public DbSet<Estoque> Estoques { get; set; }
            public DbSet<Formula> Formulas { get; set; }

        }
    }


