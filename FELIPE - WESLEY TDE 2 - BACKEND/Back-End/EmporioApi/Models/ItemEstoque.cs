﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmporioApi.Models
{
    public class ItemEstoque
    {
        public int Id { get; set; }
        public int Quantidade { get; set; }


        public Bebida Bebida { get; set; }
        public Estoque Estoque { get; set; }
    }
}
