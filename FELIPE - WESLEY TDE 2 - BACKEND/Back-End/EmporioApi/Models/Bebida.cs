﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmporioApi.Models
{
    public class Bebida
    {
        public double VolumeAlcool { get; set; }
        public string Fabricante { get; set; }
        public string Tipo { get; set; }
        public DateTime Validade { get; set; }
        public string Ingredientes { get; set; }

        public Estoque Estoque { get; set; }
        public List<ItemEstoque> ItemEstoque{ get; set; }

    }
}
