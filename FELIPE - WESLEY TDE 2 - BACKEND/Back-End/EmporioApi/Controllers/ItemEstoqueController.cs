﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmporioApi.Dados;
using EmporioApi.Models;

namespace EmporioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemEstoqueController : ControllerBase
    {
        private readonly EmporioContext _context;

        public ItemEstoqueController(EmporioContext context)
        {
            _context = context;
        }

        // GET: api/AlunosCursos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ItemEstoque>>> GetItemEstoque()
        {
            return await _context.ItemEstoque.ToListAsync();
        }

        // GET: api/AlunosCursos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ItemEstoque>> GetItemEstoque(int id)
        {
            var ItemEstoque = await _context.ItemEstoque.FindAsync(id);

            if (itemEstoque == null)
            {
                return NotFound();
            }

            return itemEstoque;
        }

        // PUT: api/AlunosCursos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutItemEstoque(int id, ItemEstoque itemEstoque)
        {
            if (id != itemEstoque.BebidaId)
            {
                return BadRequest();
            }

            _context.Entry(itemEstoque).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemEstoqueExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AlunosCursos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ItemEstoque>> PostItemEstoque(ItemEstoque itemEstoque)
        {
            _context.ItemEstoque.Add(itemEstoque);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ItemEstoqueExists(itemEstoque.BebidaId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetItemEstoque", new { id = itemEstoque.BebidaId }, itemEstoque);
        }

        // DELETE: api/AlunosCursos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ItemEstoque>> DeleteItemEstoque(int id)
        {
            var itemEstoque = await _context.ItemEstoque.FindAsync(id);
            if (itemEstoque == null)
            {
                return NotFound();
            }

            _context.ItemEstoque.Remove(itemEstoque);
            await _context.SaveChangesAsync();

            return itemEstoque;
        }

        private bool ItemEstoqueExists(int id)
        {
            return _context.ItemEstoque.Any(e => e.BebidaId == id);
        }
    }
}
