﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmporioApi.Dados;
using EmporioApi.Models;

namespace EmporioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CursosController : ControllerBase
    {
        private readonly EmporioContext _context;

        public CursosController(EmporioContext context)
        {
            _context = context;
        }

        // GET: api/Cursos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Formula>>> GetFormulas()
        {
            return await _context.Formulas.ToListAsync();
        }

        // GET: api/Cursos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Formula>> GetFormula(int id)
        {
            var formula = await _context.Formulas.FindAsync(id);

            if (formula == null)
            {
                return NotFound();
            }

            return formula;
        }

        // PUT: api/Cursos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFormula(int id, Formula formula)
        {
            if (id != formula.Id)
            {
                return BadRequest();
            }

            _context.Entry(formula).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FormulaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cursos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Curso>> PostCurso(Curso curso)
        {
            _context.Cursos.Add(curso);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCurso", new { id = curso.Id }, curso);
        }

        // DELETE: api/Cursos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Formula>> DeleteFormula(int id)
        {
            var formula = await _context.Formulas.FindAsync(id);
            if (formula == null)
            {
                return NotFound();
            }

            _context.Formulas.Remove(formula);
            await _context.SaveChangesAsync();

            return formula;
        }

        private bool FormulaExists(int id)
        {
            return _context.Formulas.Any(e => e.Id == id);
        }
    }
}
