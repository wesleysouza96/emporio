import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';

export class Estoque extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      estoques: [],
      modalAberta: false
    };

    this.buscarEstoques = this.buscarEstoques.bind(this);
    this.inserirEstoques = this.inserirEstoques.bind(this);
    this.atualizarEstoques = this.atualizarEstoques.bind(this);
    this.excluirEstoques = this.excluirEstoques.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
  
  }

  componentDidMount() {
    this.buscarEstoques();
  }


  inserirEstoques = (estoques) => {
    fetch('https://localhost:44362/api/estoques', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(estoques)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarEstoques();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarEstoques(estoques) {
    fetch('https://localhost:44362/api/estoques/' + bebidas.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(estoques)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarEstoques();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirEstoques = (id) => {
    fetch('https://localhost:44362/api/estoques/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarEstoques();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarEstoque(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const estoques = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirEstoques(estoques);
    } else {
      this.atualizarEstoques(estoques);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>ESTOQUE DE BEBIDAS</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Bebida</Form.Label>
              <Form.Control type='text' placeholder='Nome da Bebida' value={this.state.bebida} onChange={this.atualizaBebidas} />
            </Form.Group>
            
             <Form.Group>
              <Form.Label>Quantidade em Estoque</Form.Label>
              <Form.Control type='email' placeholder='Estoque' value={this.state.email} onChange={this.atualizaEstoque} />
            </Form.Group>
          </Form>
        </Modal.Body>


        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>

        


          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Volume de Alcool</th>
            <th>Fabricante</th>
            <th>Tipo</th>
            <th>Validade</th>
            <th>Ingredientes</th>
          </tr>
        </thead>
        <tbody>
          {this.state.bebidas.map((bebidas) => (
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(bebidas.id)}>ATUALIZAR</Button>
                  <Button variant="link" onClick={() => this.excluirBebida(bebidas.id)}>EXCLUIR</Button>
                </div>
              </td>
            ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>ADICIONAR BEBIDAS</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}