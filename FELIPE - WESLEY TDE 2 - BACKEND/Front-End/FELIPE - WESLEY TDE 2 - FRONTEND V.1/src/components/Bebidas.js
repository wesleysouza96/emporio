import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';

export class Bebidas extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      bebidas: [],
      modalAberta: false
    };

    this.buscarBebidas = this.buscarBebidas.bind(this);
    this.inserirBebidas = this.inserirBebidas.bind(this);
    this.atualizarBebidas = this.atualizarBebidas.bind(this);
    this.excluirBebidas = this.excluirBebidas.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
  }

  componentDidMount() {
    this.buscarBebidas();
  }

  // GET (todos alunos)
  buscarBebidas() {
    fetch('https://localhost:44362/api/bebidas')
      .then(response => response.json())
      .then(data => this.setState({ bebidas: data }));
  }
  
  //GET (aluno com determinado id)
  buscarBebidas(id) {
    fetch('https://localhost:44362/api/bebidas/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          bebida: data.bebida,
          fabricante: data.fabricante,
          tipo: data.tipo,
          validade: data.validade,
          ingredientes: data.ingredientes  
        }));
  }

  inserirBebidas = (bebidas) => {
    fetch('https://localhost:44362/api/bebidas', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(bebidas)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarBebidas();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarBebidas(bebidas) {
    fetch('https://localhost:44362/api/bebidas/' + bebidas.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(bebidas)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarBebidas();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirBebidas = (id) => {
    fetch('https://localhost:44362/api/bebidas/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarBebidas();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarBebida(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const bebidas = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirBebidas(bebidas);
    } else {
      this.atualizarBebidas(bebidas);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>PREENCHA O FORMULÁRIO SOBRE SUA BEBIDA</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>BEBIDA</Form.Label>
              <Form.Control type='text' placeholder='' value={this.state.bebida} onChange={this.atualizaBebidas} />
            </Form.Group>
            
             <Form.Group>
              <Form.Label>QUANTIDADE EM ESTOQUE</Form.Label>
              <Form.Control type='estoque' placeholder='' value={this.state.estoque} onChange={this.atualizaEstoque} />
            </Form.Group>
          </Form>
        </Modal.Body>


        <Modal.Footer>
        <Button variant="secondary" onClick={this.fecharModal}>
        ALTERAR
        </Button>

        <Button variant="primary" onClick="modalForm" type="submit">
        CONFIRMAR
        </Button>

        <Button variant="secondary" onClick={this.fecharModal}>
        SAIR
        </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Volume de Alcool</th>
            <th>Fabricante</th>
            <th>Validade</th>
            <th>Ingredientes</th>
            <th>Tipo</th>
          </tr>
        </thead>
        <tbody>
          {this.state.bebidas.map((bebidas) => (
            <tr key={bebidas.id}>
              <td>{bebidas.nome}</td>
              <td>{bebidas.email}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(bebidas.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirBebida(bebidas.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar Bebida</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}