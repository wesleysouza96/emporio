import React, { Component } from 'react';

export class Home extends Component {
  render() {
    return (
      <div><h1><center>EMPÓRIO DE BEBIDAS</center></h1>
        <p><center>APLICAÇÃO DESENVOLVIDA NA DISCIPLINA MOBILE/WEB DEVELOPMENT</center></p>
        <ul>
          <h3><center>Equipe</center></h3>
          <p><center>Felipe  José da Silva Albino</center></p>
          <p><center>Wesley de Souza Marciano</center></p>          
        </ul>
      </div>
    );
  }
}
